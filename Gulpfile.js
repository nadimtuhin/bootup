var gulp = require('gulp'),
	mainBowerFiles = require('main-bower-files'),
	runSequence = require('run-sequence'),
	gulpFilter = require('gulp-filter'),
	less = require('gulp-less'),
	sourcemaps = require('gulp-sourcemaps'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	minify = require('gulp-minify-css');


/**
 * Filters for bower main files
 */
var jsFilter = gulpFilter('**/*.js'),
	cssFilter = gulpFilter('**/*.css'),
	fontFilter = gulpFilter(['**/*.svg', '**/*.eot', '**/*.woff', '**/*.ttf']),
	imgFilter = gulpFilter(['**/*.png', '**/*.gif', '**/*.jpg']);


/**
 * public folder structure
 * @type {{js: string, css: string, images: string, fonts: string, root: string}}
 */
var public = {
	js: 'assets/js',
	css: 'assets/css',
	images: 'assets/img',
	fonts: 'assets/fonts',
	root: '',
	build: 'assets/build'
};

var matcher = {
	less: 'assets/less/*.less',
	js: 'assets/js/*.js',
	css: 'assets/css/*.css'
}


gulp.task('bower', function(){
	return gulp.src(mainBowerFiles())

		.pipe(jsFilter)
		// .pipe(sourcemaps.init())
		.pipe(uglify())
		.pipe(concat('lib.min.js'))
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest(public.build))
		.pipe(jsFilter.restore())

		.pipe(cssFilter)
		.pipe(minify())
		// .pipe(sourcemaps.init())
		.pipe(concat('lib.min.css'))
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest(public.build))
		.pipe(cssFilter.restore())

		.pipe(imgFilter)
		.pipe(gulp.dest(public.images))
		.pipe(imgFilter.restore())

		.pipe(fontFilter)
		.pipe(gulp.dest(public.fonts))
		.pipe(fontFilter.restore());
});


/**
 * Compile all less files into css file
 * and move them to public folder
 */
gulp.task('less', function(){
	return gulp.src(matcher.less)
		.pipe(less())
		.pipe(concat('app.css'))
		.pipe(gulp.dest(public.css));
});


/**
 * move all js files to build folder
 */
gulp.task('js', function(){
	return gulp.src(matcher.js)
	// .pipe(uglify())
	.pipe(concat('app.min.js')).pipe(gulp.dest(public.build));
});
/**
 * move all css files to build folder
 */
gulp.task('css', ['less'], function(){
	return gulp.src(matcher.css)
	// .pipe(minify())
	.pipe(concat('app.min.css')).pipe(gulp.dest(public.build));
});

/**
 * Watchers
 */
gulp.task('watch', function(){
	gulp.watch([matcher.less, matcher.css], ['css']);
	gulp.watch(matcher.js, ['js']);
});

/**
 * Initialize all the tasks
 */
gulp.task('initialize', function(){
	runSequence('bower', ['js', 'css']);
});


/**
 * Default task
 */
gulp.task('default', ['initialize', 'watch']);